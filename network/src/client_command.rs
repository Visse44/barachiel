use crate::{ClientMessage, MessageSerilization};
use std::io::{Cursor, Write};

#[derive(Debug, Copy, Clone)]
pub enum ClientCommand<'a> {
    Prespawn,
    Spawn,
    Name(&'a str),
}

impl<'a, 'b> MessageSerilization<'b> for ClientCommand<'a> {
    fn serilize(&self, buffer: &'b mut [u8]) -> Option<usize> {
        let mut tmp_buff = [0u8; 512];
        let mut cursor = Cursor::new(&mut tmp_buff[..]);

        match self {
            ClientCommand::Prespawn => {
                cursor.write(b"prespawn").ok()?;
            },
            ClientCommand::Spawn => {
                cursor.write(b"spawn").ok()?;
            }
            ClientCommand::Name(name) => {
                write!(cursor, "name {}", name).ok()?;
            }
        }

        let position = cursor.position() as usize;
        // Unwrap should be safe here
        let cmd = std::str::from_utf8(&tmp_buff[..position]).unwrap();

        ClientMessage::StringCmd(cmd).serilize(buffer)
    }
}
