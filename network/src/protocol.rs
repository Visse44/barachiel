use crate::{
    ClientCommand, ClientMessage, InfoString, MessageBuffer, MessageBufferError,
    MessageBufferParseError, MessageParse, MessageSerilization, Packet, PacketFlags, ServerMessage,
};
use std::io::Result;
use tokio::net::UdpSocket;

trait Timeout: std::future::Future + Sized {
    fn default_timeout(self) -> tokio::time::Timeout<Self> {
        tokio::time::timeout(std::time::Duration::from_secs(1), self)
    }
}
impl<T: std::future::Future + Sized> Timeout for T {}

const BUFFER_SIZE: usize = 4096;
const MESSAGE_SIZE: usize = 65536;

async fn send_packet(socket: &mut UdpSocket, buf: &mut [u8], packet: Packet<'_>) -> Result<usize> {
    let buf = packet.serilize(buf).unwrap();
    socket.send(buf).await
}

async fn resv_packet<'a>(socket: &mut UdpSocket, buf: &'a mut [u8]) -> Option<Packet<'a>> {
    let len = socket.recv(buf).await.ok()?;
    let buf = &buf[0..len];
    Packet::parse(buf).ok()
}

pub async fn handshake(socket: &mut UdpSocket) -> Result<()> {
    let mut resv_buf: [u8; BUFFER_SIZE] = [0u8; BUFFER_SIZE];
    let mut send_buf: [u8; BUFFER_SIZE] = [0u8; BUFFER_SIZE];

    send_packet(socket, &mut send_buf, Packet::GetChallenge).await?;

    let challenge = {
        let len = socket.recv(&mut resv_buf).default_timeout().await??;

        match Packet::parse(&resv_buf[..len]) {
            Ok(Packet::Challenge { challenge }) => challenge,
            _ => return Err(std::io::ErrorKind::InvalidData.into()),
        }
    };

    send_packet(
        socket,
        &mut send_buf,
        Packet::Connect(InfoString {
            content: vec![("protocol", b"darkplaces 3"), ("challenge", challenge)],
        }),
    )
    .await?;

    match resv_packet(socket, &mut resv_buf).default_timeout().await? {
        Some(Packet::Accept) => println!("Connection accepted"),
        p => {
            println!("Error: {:?}", p);
            return Err(std::io::ErrorKind::InvalidData.into());
        }
    }

    Ok(())
}

pub struct Client {
    name: String,
    topcolor: u8,
    bottomcolor: u8,
    rate: u32,
    rate_burstsize: u32,
    socket: UdpSocket,

    send_sequence: u32,

    send_buffer: Box<[u8; BUFFER_SIZE]>,
    send_message_buffer: Box<MessageBuffer>,
}

impl Client {
    pub async fn new<S: tokio::net::ToSocketAddrs>(server: S, name: String) -> Result<Self> {
        let mut socket = UdpSocket::bind(std::net::SocketAddrV4::new(
            std::net::Ipv4Addr::UNSPECIFIED,
            0,
        ))
        .await?;
        socket.connect(server).await?;

        handshake(&mut socket).await?;

        let topcolor = 0;
        let bottomcolor = 0;
        let rate = 160000;
        let rate_burstsize = 1024;

        let send_buffer = Box::new([0u8; BUFFER_SIZE]);
        let send_message_buffer = Box::new(MessageBuffer::new());

        Ok(Self {
            name,
            topcolor,
            bottomcolor,
            rate,
            rate_burstsize,
            socket,

            send_sequence: 0,
            send_buffer,
            send_message_buffer,
        })
    }

    pub async fn run(&mut self) -> Result<()> {
        let mut resv_buffer = Box::new([0u8; BUFFER_SIZE]);
        let mut resv_message_buffer = Box::new(MessageBuffer::new());
        loop {
            let len = self.socket.recv(&mut *resv_buffer).await?;
            let packet =
                Packet::parse(&resv_buffer[..len]).map_err(|e| std::io::ErrorKind::Other)?;
            match packet {
                Packet::Regular {
                    flags,
                    sequence,
                    data,
                } => {
                    println!(
                        "Packet recived - sequence: {:5} - size: {} - flags: {:?}  ",
                        sequence,
                        data.len(),
                        flags
                    );

                    if flags.unreliable {
                        let mut remaining = data;
                        while !remaining.is_empty() {
                            let (rest, message) = ServerMessage::parse(&remaining).unwrap();
                            remaining = rest;
                            println!("  Unreliable message: {:?}", message);
                        }
                    } else {
                        // Send ack
                        if flags.data {
                            let buf = Packet::Ack { sequence }
                                .serilize(&mut self.send_buffer[..])
                                .unwrap();
                            self.socket.send(buf).await?;
                        }

                        resv_message_buffer.add_data(data).unwrap();
                        while match resv_message_buffer.parse_message::<ServerMessage>() {
                            Ok(message) => {
                                println!("  Reliable message: {:?}", message);
                                self.handle_message(message);
                                true
                            }
                            Err(MessageBufferParseError::Empty) => {
                                // The entire buffer have been parsed
                                false
                            }
                            Err(MessageBufferParseError::NotEnoughData) => {
                                if flags.eom {
                                    println!("  EOM but not enought data to parse!");
                                    return Err(std::io::ErrorKind::InvalidData.into());
                                }
                                println!("  Not enought data to parse message.");
                                false
                            }
                            Err(MessageBufferParseError::InvalidMessage) => {
                                println!("{:?}", resv_message_buffer.get_data(10));
                                return Err(std::io::ErrorKind::InvalidData.into());
                            }
                        } {}

                        if flags.eom {
                            resv_message_buffer.reset();
                        }
                    }
                }
                _ => (),
            }

            let buffer = self.send_message_buffer.get_data(1024);
            if buffer.len() != 0 {
                let packet = Packet::Regular {
                    flags: PacketFlags {
                        data: true,
                        eom: true,
                        ..PacketFlags::none()
                    },
                    sequence: self.send_sequence,
                    data: buffer,
                };

                self.send_sequence += 1;
                let buf = packet.serilize(&mut *self.send_buffer).unwrap();
                let len = self.socket.send(buf).await?;
                println!(
                    "Sending packet {}, size: {}, sent: {}",
                    self.send_sequence,
                    buffer.len(),
                    len
                );

                self.send_message_buffer.reset();
            }
        }
    }

    fn handle_message<'a>(&mut self, message: ServerMessage<'a>) {
        match message {
            ServerMessage::SignonNum(1) => {
                self.send_player_info();
            },
            ServerMessage::SignonNum(2) => {
                self.send_message_buffer.add_message(&ClientCommand::Spawn).unwrap();
            }
            _ => (),
        };
    }

    fn send_player_info(&mut self) {
        self.send_message_buffer
            .add_message(&ClientCommand::Name(self.name.as_str()))
            .unwrap();
        self.send_message_buffer
            .add_message(&ClientCommand::Prespawn)
            .unwrap();
    }
}
