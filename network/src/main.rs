include!("lib.rs");

use bytes::{Buf, BufMut, Bytes, BytesMut};
use std::net::{Ipv4Addr, SocketAddrV4};
use std::time::Duration;
use tokio::net::{ToSocketAddrs, UdpSocket};
use tokio::time::timeout;

use std::io::{Cursor, Write};

async fn test_connect<S: ToSocketAddrs>(server_addr: S) -> std::io::Result<()> {
    println!(
        "Server address: {:?}",
        server_addr.to_socket_addrs().await?.next()
    );

    let mut client = protocol::Client::new(server_addr, "Test Client".into()).await?;
    client.run().await?;

    Ok(())
}

fn main() -> std::io::Result<()> {
    let server_addr = SocketAddrV4::new(Ipv4Addr::new(172, 19, 0, 2), 26000);

    let mut runtime = tokio::runtime::Runtime::new()?;
    runtime.block_on(test_connect(server_addr))?;

    Ok(())
}
