use crate::MessageParse;

use byteorder::{BigEndian, WriteBytesExt};
use nom::{
    branch::alt,
    bytes::complete::{tag, take, take_till, take_until},
    combinator::{all_consuming, map, map_res, opt, rest, verify},
    multi::many0,
    number::complete::{be_i16, be_u16, be_u32, be_u8, le_f32},
    sequence::{separated_pair, tuple},
    IResult,
};
use std::io::Write;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct PacketFlags {
    pub data: bool,
    pub ack: bool,
    pub nak: bool,
    pub eom: bool,
    pub unreliable: bool,
    pub crypto0: bool,
    pub crypto1: bool,
    pub crypto2: bool,
    pub ctl: bool,
}

impl PacketFlags {
    pub fn none() -> Self {
        Self {
            data: false,
            ack: false,
            nak: false,
            eom: false,
            unreliable: false,
            crypto0: false,
            crypto1: false,
            crypto2: false,
            ctl: false,
        }
    }
}

const PACKET_FLAG_DATA: u16 = 0x0001;
const PACKET_FLAG_ACK: u16 = 0x0002;
const PACKET_FLAG_NAK: u16 = 0x0004;
const PACKET_FLAG_EOM: u16 = 0x0008;
const PACKET_FLAG_UNRELIABLE: u16 = 0x0010;
const PACKET_FLAG_CRYPTO0: u16 = 0x1000;
const PACKET_FLAG_CRYPTO1: u16 = 0x2000;
const PACKET_FLAG_CRYPTO2: u16 = 0x4000;
const PACKET_FLAG_CTL: u16 = 0x8000;

impl From<u16> for PacketFlags {
    fn from(flags: u16) -> Self {
        Self {
            data: (flags & PACKET_FLAG_DATA) != 0,
            ack: (flags & PACKET_FLAG_ACK) != 0,
            nak: (flags & PACKET_FLAG_NAK) != 0,
            eom: (flags & PACKET_FLAG_EOM) != 0,
            unreliable: (flags & PACKET_FLAG_UNRELIABLE) != 0,
            crypto0: (flags & PACKET_FLAG_CRYPTO0) != 0,
            crypto1: (flags & PACKET_FLAG_CRYPTO1) != 0,
            crypto2: (flags & PACKET_FLAG_CRYPTO2) != 0,
            ctl: (flags & PACKET_FLAG_CTL) != 0,
        }
    }
}

impl Into<u16> for PacketFlags {
    fn into(self) -> u16 {
        let mut flags = 0;
        if self.data {
            flags |= PACKET_FLAG_DATA
        }
        if self.ack {
            flags |= PACKET_FLAG_ACK
        }
        if self.nak {
            flags |= PACKET_FLAG_NAK
        }
        if self.eom {
            flags |= PACKET_FLAG_EOM
        }
        if self.unreliable {
            flags |= PACKET_FLAG_UNRELIABLE
        }
        if self.crypto0 {
            flags |= PACKET_FLAG_CRYPTO0
        }
        if self.crypto1 {
            flags |= PACKET_FLAG_CRYPTO1
        }
        if self.crypto2 {
            flags |= PACKET_FLAG_CRYPTO2
        }
        if self.ctl {
            flags |= PACKET_FLAG_CTL
        }
        flags
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct InfoString<'a> {
    pub content: Vec<(&'a str, &'a [u8])>,
}

impl<'a> InfoString<'a> {
    pub fn get_value(&self, name: &str) -> Option<&'a [u8]> {
        for (n, v) in self.content.iter() {
            if name == *n {
                return Some(v);
            }
        }
        None
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Packet<'a> {
    // Client -> Server packets
    GetChallenge,
    Connect(InfoString<'a>),

    // Server -> Client packets
    Challenge {
        challenge: &'a [u8],
    },
    Accept,
    Reject {
        reason: &'a str,
    },

    // Both ways,
    Ack {
        sequence: u32,
    },
    Regular {
        flags: PacketFlags,
        sequence: u32,
        data: &'a [u8],
    },
}

impl<'a> Packet<'a> {
    fn parse_ctl_get_challange(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, _) = tag(b"getchallenge")(input)?;
        Ok((input, Packet::GetChallenge))
    }

    fn parse_ctl_connect(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, _) = tag(b"connect")(input)?;

        fn info_string<'a>(input: &'a [u8]) -> IResult<&'a [u8], (&'a str, &'a [u8])> {
            let (input, _) = tag(b"\\")(input)?;
            separated_pair(
                map_res(take_until(&b"\\"[..]), |s| std::str::from_utf8(s)),
                tag(b"\\"),
                alt((take_until(&b"\\"[..]), rest)),
            )(input)
        }

        let (input, content) = many0(info_string)(input)?;

        Ok((input, Self::Connect(InfoString { content })))
    }

    fn parse_ctl_challenge(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, _) = tag(b"challenge ")(input)?;
        let (input, challenge) = rest(input)?;

        Ok((input, Packet::Challenge { challenge }))
    }

    fn parse_ctl_accept(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, _) = tag(b"accept")(input)?;

        Ok((input, Packet::Accept))
    }

    fn parse_ctl_reject(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, _) = tag(b"reject ")(input)?;
        let (input, reason) = map_res(rest, |s| std::str::from_utf8(s))(input)?;

        Ok((input, Packet::Reject { reason }))
    }

    pub fn parse_ctl(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, _) = tag(b"\xFF\xFF\xFF\xFF")(input)?;

        // Don't allow backtracing when
        match alt((
            Self::parse_ctl_get_challange,
            Self::parse_ctl_connect,
            Self::parse_ctl_challenge,
            Self::parse_ctl_accept,
            Self::parse_ctl_reject,
        ))(input)
        {
            Err(nom::Err::Error(e)) => Err(nom::Err::Failure(e)),
            val => val,
        }
    }

    pub fn parse_ack(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, (_, _, sequence)) =
            all_consuming(tuple((tag(b"\x00\x02"), tag(b"\x00\x08"), be_u32)))(input)?;

        Ok((input, Self::Ack { sequence }))
    }

    pub fn parse_regular(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, (flags, _length, sequence, data)) =
            tuple((map(be_u16, |f| PacketFlags::from(f)), be_u16, be_u32, rest))(input)?;

        Ok((
            input,
            Self::Regular {
                flags,
                sequence,
                data,
            },
        ))
    }

    pub fn parse(input: &'a [u8]) -> Result<Self, nom::Err<(&'a [u8], nom::error::ErrorKind)>> {
        all_consuming(alt((Self::parse_ctl, Self::parse_ack, Self::parse_regular)))(input)
            .map(|(i, packet)| packet)
    }

    pub fn serilize(&self, buffer: &'a mut [u8]) -> Option<&'a [u8]> {
        let mut cursor = std::io::Cursor::new(buffer);

        match self {
            Self::GetChallenge => {
                cursor.write_u32::<BigEndian>(0xFFFFFFFF).ok()?;
                cursor.write(b"getchallenge").ok()?;
            }
            Self::Connect(info_string) => {
                cursor.write_u32::<BigEndian>(0xFFFFFFFF).ok()?;
                cursor.write(b"connect").ok()?;

                for (name, value) in info_string.content.iter() {
                    cursor.write_u8(b'\\').ok()?;
                    cursor.write(name.as_bytes()).ok()?;
                    cursor.write_u8(b'\\').ok()?;
                    assert!(value.contains(&b'\\') == false);
                    cursor.write(*value).ok()?;

                    println!("{:?} = {:?}", name, std::str::from_utf8(value));
                }
            }
            Self::Challenge { challenge } => {
                cursor.write_u32::<BigEndian>(0xFFFFFFFF).ok()?;

                cursor.write(b"challenge ").ok()?;
                cursor.write(challenge).ok()?;
            }
            Self::Accept => {
                cursor.write_u32::<BigEndian>(0xFFFFFFFF).ok()?;

                cursor.write(b"accept").ok()?;
            }
            Self::Reject { reason } => {
                cursor.write_u32::<BigEndian>(0xFFFFFFFF).ok()?;

                cursor.write(b"reject ").ok()?;
                cursor.write(reason.as_bytes()).ok()?;
            }
            Self::Ack { sequence } => {
                cursor.write_u16::<BigEndian>(0x0002).ok()?;
                cursor.write_u16::<BigEndian>(0x0008).ok()?;
                cursor.write_u32::<BigEndian>(*sequence).ok()?;
            }
            Self::Regular {
                flags,
                sequence,
                data,
            } => {
                let length = 8 + data.len() as u16;
                cursor.write_u16::<BigEndian>((*flags).into()).ok()?;
                cursor.write_u16::<BigEndian>(length).ok()?;
                cursor.write_u32::<BigEndian>(*sequence).ok()?;

                cursor.write(*data).ok()?;
            }
        }

        let position = cursor.position() as usize;
        Some(&cursor.into_inner()[..position])
    }
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    mod Packet {
        use super::super::{InfoString, Packet};

        fn test_packet(raw: &[u8], result: Packet) {
            assert_eq!(Packet::parse(raw), Ok(result.clone()), "Parsing failed");

            let mut buffer = [0u8; 4096];

            assert_eq!(
                result.serilize(&mut buffer),
                Some(raw),
                "Serilization failed"
            );
        }

        #[test]
        fn GetChallange() {
            test_packet(b"\xFF\xFF\xFF\xFFgetchallenge", Packet::GetChallenge);
        }

        #[test]
        fn Challange() {
            test_packet(
                b"\xff\xff\xff\xff\x63\x68\x61\x6c\x6c\x65\x6e\x67\x65\x20\x74\x7c\x59\x3f\x56\x58\x44\x74\x77\x3d\x47\x00\x76\x6c\x65\x6e\x2e\x00\x00\x00\x64\x30\x70\x6b\x58\x6f\x6e\x2f\x2f\x4b\x73\x73\x64\x6c\x7a\x47\x6b\x46\x4b\x64\x6e\x6e\x4e\x34\x73\x67\x67\x38\x48\x2b\x6b\x6f\x54\x62\x42\x6e\x35\x4a\x54\x69\x33\x37\x42\x41\x57\x31\x51\x3d\x00\x00",
                Packet::Challenge{challenge: b"t|Y?VXDtw=G\x00vlen\x2e\x00\x00\x00d0pkXon//KssdlzGkFKdnnN4sgg8H+koTbBn5JTi37BAW1Q=\x00\x00"}
            );
        }

        #[test]
        fn Connect() {
            test_packet(
                b"\xff\xff\xff\xff\x63\x6f\x6e\x6e\x65\x63\x74\x5c\x70\x72\x6f\x74\x6f\x63\x6f\x6c\x5c\x64\x61\x72\x6b\x70\x6c\x61\x63\x65\x73\x20\x33\x5c\x70\x72\x6f\x74\x6f\x63\x6f\x6c\x73\x5c\x44\x50\x37\x5c\x63\x68\x61\x6c\x6c\x65\x6e\x67\x65\x5c\x74\x7c\x59\x3f\x56\x58\x44\x74\x77\x3d\x47\x00\x76\x6c\x65\x6e\x2e\x00\x00\x00\x64\x30\x70\x6b\x58\x6f\x6e\x2f\x2f\x4b\x73\x73\x64\x6c\x7a\x47\x6b\x46\x4b\x64\x6e\x6e\x4e\x34\x73\x67\x67\x38\x48\x2b\x6b\x6f\x54\x62\x42\x6e\x35\x4a\x54\x69\x33\x37\x42\x41\x57\x31\x51\x3d\x00\x00",
                Packet::Connect(InfoString{
                    content: vec![
                        ("protocol", b"darkplaces 3"),
                        ("protocols", b"DP7"),
                        ("challenge", &[116, 124, 89, 63, 86, 88, 68, 116, 119, 61, 71, 0, 118, 108, 101, 110, 46, 0, 0, 0, 100, 48, 112, 107, 88, 111, 110, 47, 47, 75, 115, 115, 100, 108, 122, 71, 107, 70, 75, 100, 110, 110, 78, 52, 115, 103, 103, 56, 72, 43, 107, 111, 84, 98, 66, 110, 53, 74, 84, 105, 51, 55, 66, 65, 87, 49, 81, 61, 0, 0])
                    ]
                })
            );
        }

        #[test]
        fn Accept() {
            test_packet(b"\xFF\xFF\xFF\xFFaccept", Packet::Accept);
        }

        #[test]
        fn Reject() {
            test_packet(
                b"\xFF\xFF\xFF\xFFreject Rejection reason.",
                Packet::Reject {
                    reason: "Rejection reason.",
                },
            );
        }

        #[test]
        fn Ack() {
            test_packet(
                b"\x00\x02\x00\x08\x12\x34\x56\x78",
                Packet::Ack {
                    sequence: 0x12345678,
                },
            );
        }
        /*
        #[test]
        fn Regular() {
            println!(
            test_packet(
                b"",
                Packet::Accept
            )
        }*/

        // #[test]
        // fn test()
        // {
        //     let input = b"\x10\xab\x91\xd9\x8b\xa5\x32\xf5\x18\x4f\x3a\x57\x99\x14\x1d\xf3\x60\x02\x00\x08\x00\x00\x00\x17";
        //     println!("{:?}", Packet::parse(input));
        //     assert!(false);
        // }
    }
}
