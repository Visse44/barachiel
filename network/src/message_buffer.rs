use crate::message::{MessageParse, MessageSerilization};

#[derive(Debug, Copy, Clone)]
pub enum MessageBufferError {
    BufferOverflow,
    MessageSerilizationError,
    InvalidMessage,
    NotEnoughData,
}

#[derive(Debug, thiserror::Error)]
pub enum MessageBufferParseError {
    #[error("Empty")]
    Empty,

    #[error("Invalid message")]
    InvalidMessage,

    #[error("Not enought data")]
    NotEnoughData,
}

const BUFFER_SIZE: usize = 65536;

pub struct MessageBuffer {
    write_offset: usize,
    read_offset: usize,

    buffer: [u8; BUFFER_SIZE],
}

impl MessageBuffer {
    pub fn new() -> Self {
        Self {
            write_offset: 0,
            read_offset: 0,
            buffer: [0u8; BUFFER_SIZE],
        }
    }

    pub fn add_data(&mut self, data: &[u8]) -> Result<(), MessageBufferError> {
        let end = self.write_offset + data.len();
        if end > BUFFER_SIZE {
            Err(MessageBufferError::BufferOverflow)
        } else {
            self.buffer[self.write_offset..end].copy_from_slice(data);
            self.write_offset += data.len();
            Ok(())
        }
    }

    pub fn get_data<'a>(&'a mut self, max_len: usize) -> &'a [u8] {
        let read_len = max_len.min(self.write_offset - self.read_offset);
        let start = self.read_offset;
        let end = start + read_len;
        self.read_offset += read_len;

        &self.buffer[start..end]
    }

    pub fn add_message<'a, M: MessageSerilization<'a>>(
        &'a mut self,
        message: &M,
    ) -> Result<(), MessageBufferError> {
        let remaining = message
            .serilize(&mut self.buffer[self.write_offset..])
            .ok_or(MessageBufferError::MessageSerilizationError)?;
        self.write_offset += remaining;
        Ok(())
    }

    pub fn parse_message<'a, M: MessageParse<'a>>(
        &'a mut self,
    ) -> Result<M, MessageBufferParseError> {
        if self.read_offset == self.write_offset {
            return Err(MessageBufferParseError::Empty);
        }

        match M::parse(&self.buffer[self.read_offset..self.write_offset]) {
            Ok((input, message)) => {
                let original_len = self.write_offset - self.read_offset;
                let remaining_len = input.len();

                let consumed = original_len - remaining_len;
                self.read_offset += consumed;
                Ok(message)
            }
            Err(nom::Err::Incomplete(_)) => Err(MessageBufferParseError::NotEnoughData),
            Err(_) => Err(MessageBufferParseError::InvalidMessage),
        }
    }

    pub fn reset(&mut self) {
        self.write_offset = 0;
        self.read_offset = 0;
    }
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    use super::*;

    #[test]
    fn MessageBuffer() {
        let mut buffer = MessageBuffer::new();
        buffer.add_data(&[0x00, 0x01, 0x02, 0x03]).unwrap();
        assert_eq!(buffer.get_data(10), [0x00, 0x01, 0x02, 0x03]);

        // @todo more tests
    }
}
