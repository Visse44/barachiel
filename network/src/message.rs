use byteorder::{BigEndian, LittleEndian, WriteBytesExt};
use nom::{
    branch::alt,
    bytes::streaming::{tag, take_until},
    call,
    combinator::{map, map_res, verify},
    multi::many_till,
    number::streaming::{be_f32, be_i16, be_u16, be_u32, be_u8, le_f32},
    sequence::tuple,
    switch, IResult,
};
use std::io::Write;

pub trait MessageParse<'a>: Sized {
    fn parse(input: &'a [u8]) -> IResult<&'a [u8], Self>;
}

pub trait MessageSerilization<'a>: Sized {
    fn serilize(&self, buffer: &'a mut [u8]) -> Option<usize>;
}

fn parse_string<'a>(input: &'a [u8]) -> IResult<&'a [u8], &'a str> {
    let (input, value) = map_res(take_until(&b"\x00"[..]), |i| std::str::from_utf8(i))(input)?;
    let (input, _) = tag("\x00")(input)?;

    Ok((input, value))
}

fn parse_angle16<'a>(input: &'a [u8]) -> IResult<&'a [u8], f32> {
    let (input, angle) = be_i16(input)?;
    let angle = (angle as f32) * (360.0 / 65536.0);

    Ok((input, angle))
}

fn write_angle16<W: WriteBytesExt>(dst: &mut W, angle: f32) -> std::io::Result<()> {
    let angle = if angle >= 0.0 {
        (angle * (65536.0 / 360.0) + 0.5) as i32
    } else {
        (angle * (65536.0 / 360.0) - 0.5) as i32
    };

    dst.write_i16::<BigEndian>(angle as i16)
}

fn parse_coord16i<'a>(input: &'a [u8]) -> IResult<&'a [u8], f32> {
    let (input, coord) = be_i16(input)?;
    Ok((input, coord as f32))
}

fn write_coord16i<W: WriteBytesExt>(dst: &mut W, coord: f32) -> std::io::Result<()> {
    let coord = if coord >= 0.0 {
        (coord + 0.5) as i16
    } else {
        (coord - 0.5) as i16
    };
    dst.write_i16::<BigEndian>(coord)
}

#[derive(Debug, Clone, PartialEq)]
pub enum ClientMessage<'a> {
    Nop,
    Disconnect,
    Move {
        sequence: u32,
        time: f32,
        view_angle: (f32, f32, f32),
        forward: f32,
        sideways: f32,
        vertical: f32,
        buttons: u32,
        impulse: u8,
        cursor_screen: (f32, f32),
        cursor_start: (f32, f32, f32),
        cursor_impact: (f32, f32, f32),
        cursor_entity: u16,
    },
    StringCmd(&'a str),
}

impl<'a> ClientMessage<'a> {
    fn parse_nop(input: &'a [u8]) -> IResult<&'a [u8], ClientMessage<'a>> {
        Ok((input, Self::Nop))
    }

    fn parse_disconnect(input: &'a [u8]) -> IResult<&'a [u8], ClientMessage<'a>> {
        Ok((input, Self::Disconnect))
    }

    fn parse_move(input: &'a [u8]) -> IResult<&'a [u8], ClientMessage<'a>> {
        let (input, sequence) = be_u32(input)?;
        let (input, time) = be_f32(input)?;
        let (input, view_angle) = tuple((parse_angle16, parse_angle16, parse_angle16))(input)?;
        let (input, forward) = parse_coord16i(input)?;
        let (input, sideways) = parse_coord16i(input)?;
        let (input, vertical) = parse_coord16i(input)?;
        let (input, buttons) = be_u32(input)?;
        let (input, impulse) = be_u8(input)?;
        let (input, cursor_screen) = tuple((be_i16, be_i16))(input)?;
        let (input, cursor_start) = tuple((be_f32, be_f32, be_f32))(input)?;
        let (input, cursor_impact) = tuple((be_f32, be_f32, be_f32))(input)?;
        let (input, cursor_entity) = be_u16(input)?;

        let cursor_screen = {
            let (x, y) = cursor_screen;
            (x as f32 / 32767.0, y as f32 / 32767.0)
        };

        Ok((
            input,
            Self::Move {
                sequence,
                time,
                view_angle,
                forward,
                sideways,
                vertical,
                buttons,
                impulse,
                cursor_screen,
                cursor_start,
                cursor_impact,
                cursor_entity,
            },
        ))
    }

    fn parse_string_cmd(input: &'a [u8]) -> IResult<&'a [u8], ClientMessage<'a>> {
        let (input, cmd) = parse_string(input)?;
        Ok((input, Self::StringCmd(cmd)))
    }
}

impl<'a> MessageParse<'a> for ClientMessage<'a> {
    fn parse(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        switch!(input, call!(be_u8),
            0x01 => call!(Self::parse_nop) |
            0x02 => call!(Self::parse_disconnect) |
            0x03 => call!(Self::parse_move) |
            0x04 => call!(Self::parse_string_cmd)
        )
    }
}

impl<'a> MessageSerilization<'a> for ClientMessage<'a> {
    fn serilize(&self, buffer: &'a mut [u8]) -> Option<usize> {
        let mut cursor = std::io::Cursor::new(buffer);
        match self {
            Self::Nop => {
                cursor.write_u8(0x01).ok()?;
            }
            Self::Disconnect => {
                cursor.write_u8(0x02).ok()?;
            }
            Self::Move {
                sequence,
                time,
                view_angle,
                forward,
                sideways,
                vertical,
                buttons,
                impulse,
                cursor_screen,
                cursor_start,
                cursor_impact,
                cursor_entity,
            } => {
                cursor.write_u8(0x03).ok()?;
                cursor.write_u32::<BigEndian>(*sequence).ok()?;
                cursor.write_f32::<BigEndian>(*time).ok()?;
                write_angle16(&mut cursor, view_angle.0).ok()?;
                write_angle16(&mut cursor, view_angle.1).ok()?;
                write_angle16(&mut cursor, view_angle.2).ok()?;
                write_coord16i(&mut cursor, *forward).ok()?;
                write_coord16i(&mut cursor, *sideways).ok()?;
                write_coord16i(&mut cursor, *vertical).ok()?;
                cursor.write_u32::<BigEndian>(*buttons).ok()?;
                cursor.write_u8(*impulse).ok()?;
                cursor
                    .write_i16::<BigEndian>((cursor_screen.0 * 32767.0) as i16)
                    .ok()?;
                cursor
                    .write_i16::<BigEndian>((cursor_screen.1 * 32767.0) as i16)
                    .ok()?;
                cursor.write_f32::<BigEndian>(cursor_start.0).ok()?;
                cursor.write_f32::<BigEndian>(cursor_start.1).ok()?;
                cursor.write_f32::<BigEndian>(cursor_start.2).ok()?;
                cursor.write_f32::<BigEndian>(cursor_impact.0).ok()?;
                cursor.write_f32::<BigEndian>(cursor_impact.1).ok()?;
                cursor.write_f32::<BigEndian>(cursor_impact.2).ok()?;
                cursor.write_u16::<BigEndian>(*cursor_entity).ok()?;
            }
            Self::StringCmd(cmd) => {
                cursor.write_u8(0x04).ok()?;
                cursor.write(cmd.as_bytes()).ok()?;
                cursor.write_u8(0x00).ok()?;
            }
        }

        Some(cursor.position() as usize)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum ServerMessage<'a> {
    Nop,
    Disconnect,
    UpdateStat {
        stat: u8,
        value: u32,
    },
    Version(u32),
    SetView(u16),
    Sound {
        sound: u16,
        volume: Option<u8>,
        attenuation: Option<f32>,
        speed: Option<f32>,
        entity: u16,
        channel: u8,
        looping: bool,
    },
    Time(f32),
    Print(&'a str),
    StuffText(&'a str),
    SetAngle(f32, f32, f32),
    ServerInfo {
        protocol: u32,
        max_clients: u8,
        game_type: u8,
        world_message: &'a str,
        models: Vec<&'a str>,
        sounds: Vec<&'a str>,
    },
    LightStyle {
        index: u8,
        style: &'a str,
    },
    UpdateName {
        client: u8,
        name: &'a str,
    },
    UpdateFrags {
        client: u8,
        frags: u16,
    },
    ClientData,
    StopSound {
        entity: u16,
        channel: u8,
    },
    UpdateColors,
    Particle,
    Damage,
    SpawnStatic,
    SpawnBaseline,
    TempEntity,
    SetPause,
    SignonNum(u8),
    CenterPrint,
    KilledMonster,
    FoundSecret,
    SpawnStaticSound {
        sound: u16,
        origin: (f32, f32, f32),
        volume: u8,
        attenuation: u8,
    },
    Intermission,
    Finale,
    CDTrack {
        track: u8,
        looptrack: u8,
    },
    Sellscreen,
    Cutscene,
    ShowLMP,
    HideLMP,
    Skybox,

    DownloadData,
    UpdateStatusByte,
    Effect,
    // Effec2,
    Precache,
    // SpawnBaseline2,
    // Static2,
    Entities,
    CsqcEntities,
    TrailParticles,
    PointParticles,
}

const SOUND_FIELD_FLAG_VOLUME: u8 = 1 << 0;
const SOUND_FIELD_FLAG_ATTENUATION: u8 = 1 << 1;
const SOUND_FIELD_FLAG_LOOPING: u8 = 1 << 2;
const SOUND_FIELD_FLAG_LARGE_ENTITY: u8 = 1 << 3;
const SOUND_FIELD_FLAG_LARGE_SOUND: u8 = 1 << 4;
const SOUND_FIELD_FLAG_SPEEDUSHORT4000: u8 = 1 << 5;

const CLIENT_DATA_FLAG_VIEWHEIGHT: u32 = 1 << 0;
const CLIENT_DATA_FLAG_IDEALPITCH: u32 = 1 << 1;
const CLIENT_DATA_FLAG_PUNCH1: u32 = 1 << 2;
const CLIENT_DATA_FLAG_PUNCH2: u32 = 1 << 3;
const CLIENT_DATA_FLAG_PUNCH3: u32 = 1 << 4;
const CLIENT_DATA_FLAG_VELOCITY1: u32 = 1 << 5;
const CLIENT_DATA_FLAG_VELOCITY2: u32 = 1 << 6;
const CLIENT_DATA_FLAG_VELOCITY3: u32 = 1 << 7;
const CLIENT_DATA_FLAG_UNUSED8: u32 = 1 << 8;
const CLIENT_DATA_FLAG_ITEMS: u32 = 1 << 9;
const CLIENT_DATA_FLAG_ONGROUND: u32 = 1 << 10;
const CLIENT_DATA_FLAG_INWATER: u32 = 1 << 11;
const CLIENT_DATA_FLAG_WEAPONFRAME: u32 = 1 << 12;
const CLIENT_DATA_FLAG_ARMOR: u32 = 1 << 13;
const CLIENT_DATA_FLAG_WEAPON: u32 = 1 << 14;
const CLIENT_DATA_FLAG_EXTEND1: u32 = 1 << 15;
const CLIENT_DATA_FLAG_PUNCHVEC1: u32 = 1 << 16;
const CLIENT_DATA_FLAG_PUNCHVEC2: u32 = 1 << 17;
const CLIENT_DATA_FLAG_PUNCHVEC3: u32 = 1 << 18;
const CLIENT_DATA_FLAG_VIEWZOOM: u32 = 1 << 19;
const CLIENT_DATA_FLAG_UNUSED20: u32 = 1 << 20;
const CLIENT_DATA_FLAG_UNUSED21: u32 = 1 << 21;
const CLIENT_DATA_FLAG_UNUSED22: u32 = 1 << 22;
const CLIENT_DATA_FLAG_EXTEND2: u32 = 1 << 23;
const CLIENT_DATA_FLAG_UNUSED24: u32 = 1 << 24;
const CLIENT_DATA_FLAG_UNUSED25: u32 = 1 << 25;
const CLIENT_DATA_FLAG_UNUSED26: u32 = 1 << 26;
const CLIENT_DATA_FLAG_UNUSED27: u32 = 1 << 27;
const CLIENT_DATA_FLAG_UNUSED28: u32 = 1 << 28;
const CLIENT_DATA_FLAG_UNUSED29: u32 = 1 << 29;
const CLIENT_DATA_FLAG_UNUSED30: u32 = 1 << 30;
const CLIENT_DATA_FLAG_EXTEND3: u32 = 1 << 31;

impl<'a> ServerMessage<'a> {
    fn parse_nop(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        Ok((input, Self::Nop))
    }

    fn parse_disconnect(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        Ok((input, Self::Disconnect))
    }

    fn parse_update_stat(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, stat) = be_u8(input)?;
        let (input, value) = be_u32(input)?;

        Ok((input, Self::UpdateStat { stat, value }))
    }

    fn parse_version(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, version) = be_u32(input)?;

        Ok((input, Self::Version(version)))
    }

    fn parse_set_view(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, view) = be_u16(input)?;
        Ok((input, Self::SetView(view)))
    }

    fn parse_sound(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, fields) = {
            verify(be_u8, |fields| {
                (fields
                    & !(SOUND_FIELD_FLAG_VOLUME
                        | SOUND_FIELD_FLAG_ATTENUATION
                        | SOUND_FIELD_FLAG_LOOPING
                        | SOUND_FIELD_FLAG_LARGE_ENTITY
                        | SOUND_FIELD_FLAG_LARGE_SOUND
                        | SOUND_FIELD_FLAG_SPEEDUSHORT4000))
                    == 0
            })(input)?
        };

        let looping = (fields & SOUND_FIELD_FLAG_LOOPING) != 0;

        let (input, volume) = if (fields & SOUND_FIELD_FLAG_VOLUME) != 0 {
            map(be_u8, |v| Some(v))(input)?
        } else {
            (input, None)
        };

        let (input, attenuation) = if (fields & SOUND_FIELD_FLAG_ATTENUATION) != 0 {
            map(be_u8, |num| Some((num as f32) / 64.0))(input)?
        } else {
            (input, None)
        };

        let (input, speed) = if (fields & SOUND_FIELD_FLAG_SPEEDUSHORT4000) != 0 {
            map(be_u16, |num| Some((num as f32) / 4000.0))(input)?
        } else {
            (input, None)
        };

        let (input, (entity, channel)) = if (fields & SOUND_FIELD_FLAG_LARGE_ENTITY) != 0 {
            tuple((be_u16, be_u8))(input)?
        } else {
            let (input, entchan) = be_u16(input)?;
            let entity = entchan >> 3;
            let channel = (entchan & 0x7) as u8;

            (input, (entity, channel))
        };

        let (input, sound) = if (fields & SOUND_FIELD_FLAG_LARGE_SOUND) != 0 {
            be_u16(input)?
        } else {
            map(be_u8, |v| v as u16)(input)?
        };

        Ok((
            input,
            Self::Sound {
                sound,
                volume,
                attenuation,
                speed,
                entity,
                channel,
                looping,
            },
        ))
    }

    fn parse_time(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        // @todo make sure that its actually a little endian float, (seems like most other parts use big endian...)
        let (input, time) = le_f32(input)?;

        Ok((input, Self::Time(time)))
    }

    fn parse_print(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, string) = parse_string(input)?;
        Ok((input, Self::Print(string)))
    }

    fn parse_stuff_text(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, string) = parse_string(input)?;
        Ok((input, Self::StuffText(string)))
    }

    fn parse_set_angle(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, (a1, a2, a3)) = tuple((parse_angle16, parse_angle16, parse_angle16))(input)?;

        Ok((input, Self::SetAngle(a1, a2, a3)))
    }

    fn parse_server_info(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, protocol) = be_u32(input)?;
        let (input, max_clients) = be_u8(input)?;
        let (input, game_type) = be_u8(input)?;
        let (input, world_message) = parse_string(input)?;
        let (input, (models, _)) = many_till(parse_string, tag(b"\x00"))(input)?;
        let (input, (sounds, _)) = many_till(parse_string, tag(b"\x00"))(input)?;

        Ok((
            input,
            Self::ServerInfo {
                protocol,
                max_clients,
                game_type,
                world_message,
                models,
                sounds,
            },
        ))
    }

    fn parse_light_style(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, index) = be_u8(input)?;
        let (input, style) = parse_string(input)?;

        Ok((input, Self::LightStyle { index, style }))
    }

    fn parse_update_name(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, client) = be_u8(input)?;
        let (input, name) = parse_string(input)?;

        Ok((input, Self::UpdateName { client, name }))
    }

    fn parse_update_frags(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, client) = be_u8(input)?;
        let (input, frags) = be_u16(input)?;

        Ok((input, Self::UpdateFrags { client, frags }))
    }

    fn parse_client_data(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, bits) = be_u16(input)?;
        let bits = bits as u32;

        todo!("ServerMessage::ClientData");
    }

    fn parse_stop_sound(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, entchan) = be_u16(input)?;

        let entity = entchan >> 3;
        let channel = (entchan & 0x7) as u8;

        Ok((input, Self::StopSound { entity, channel }))
    }

    fn parse_update_colors(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::UpdateColors");
    }

    fn parse_particle(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Particle");
    }

    fn parse_damage(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Damage");
    }

    fn parse_spawn_static(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::SpawnStatic");
    }

    fn parse_spawn_baseline(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::SpawnBaseline");
    }

    fn parse_temp_entity(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        // hacky, but try to "jump" the entity by seeing if we can parse anything after it
        let (input, entity) = be_u8(input)?;
        println!("WARNING: Cant parse TempEntity, trying to workaround... (entity is {})", entity);
        for length in 20..64 {
            match Self::parse(&input[length..]) {
                Ok(_) => { // We found an offset that worked, go with it!
                    println!("WARNING: Found a working offset of {}", length);
                    return Ok((&input[length..], Self::TempEntity));
                }
                Err(nom::Err::Incomplete(_)) => {
                    // It could work if we had a bit more data
                    return Err(nom::Err::Incomplete(nom::Needed::Unknown))
                }
                Err(_) => {
                    // It failed, try another offset
                }
            }
        }
        return Err(nom::Err::Error((input, nom::error::ErrorKind::Tag)));
    }

    fn parse_set_pause(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::SetPause");
    }

    fn parse_signon_num(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, stage) = be_u8(input)?;
        Ok((input, ServerMessage::SignonNum(stage)))
    }

    fn parse_center_print(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::CenterPrint");
    }

    fn parse_killed_monster(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::KilledMonster");
    }

    fn parse_found_secret(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::FoundSecret");
    }

    fn parse_spawn_static_sound(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, origin) = tuple((le_f32, le_f32, le_f32))(input)?;
        let (input, sound) = map(be_u8, |v| v as u16)(input)?;
        let (input, volume) = be_u8(input)?;
        let (input, attenuation) = be_u8(input)?;

        Ok((
            input,
            Self::SpawnStaticSound {
                sound,
                origin,
                volume,
                attenuation,
            },
        ))
    }

    fn parse_intermission(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Intermission");
    }

    fn parse_finale(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Finale");
    }

    fn parse_cd_track(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, track) = be_u8(input)?;
        let (input, looptrack) = be_u8(input)?;

        Ok((input, Self::CDTrack { track, looptrack }))
    }

    fn parse_sellscreen(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Sellscreen");
    }

    fn parse_cutscreen(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Cutscene");
    }

    fn parse_show_lmp(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::ShowLMP");
    }

    fn parse_hide_lmp(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::HideLMP");
    }

    fn parse_skybox(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Skybox");
    }

    fn parse_download_data(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::DownloadData");
    }

    fn parse_update_status_byte(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::UpdateStatusByte");
    }

    fn parse_effect(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Effect");
    }

    fn parse_effect2(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Effect2");
    }

    fn parse_precache(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Precache");
    }

    fn parse_spawn_baseline2(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::SpawnBaseline2");
    }

    fn parse_spawn_static2(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Static2");
    }

    fn parse_entities(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::Entities");
    }

    fn parse_csqc_entities(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::CsqcEntities");
    }

    fn parse_spawn_static_sound2(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        let (input, origin) = tuple((le_f32, le_f32, le_f32))(input)?;
        let (input, sound) = be_u16(input)?;
        let (input, volume) = be_u8(input)?;
        let (input, attenuation) = be_u8(input)?;

        Ok((
            input,
            Self::SpawnStaticSound {
                sound,
                origin,
                volume,
                attenuation,
            },
        ))
    }

    fn parse_trail_particles(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::TrailParticles");
    }

    fn parse_point_particles(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::PointParticles");
    }

    fn parse_point_particles1(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        todo!("ServerMessage::PointParticles1");
    }
}

impl<'a> MessageParse<'a> for ServerMessage<'a> {
    fn parse(input: &'a [u8]) -> IResult<&'a [u8], Self> {
        switch!(input, call!(be_u8),
            0x01 => call!(Self::parse_nop) |
            0x02 => call!(Self::parse_disconnect) |
            0x03 => call!(Self::parse_update_stat) |
            0x04 => call!(Self::parse_version) |
            0x05 => call!(Self::parse_set_view) |
            0x06 => call!(Self::parse_sound) |
            0x07 => call!(Self::parse_time) |
            0x08 => call!(Self::parse_print) |
            0x09 => call!(Self::parse_stuff_text) |
            0x0A => call!(Self::parse_set_angle) |
            0x0B => call!(Self::parse_server_info) |
            0x0C => call!(Self::parse_light_style) |
            0x0D => call!(Self::parse_update_name) |
            0x0E => call!(Self::parse_update_frags) |
            0x0F => call!(Self::parse_client_data) |
            0x10 => call!(Self::parse_stop_sound) |
            0x11 => call!(Self::parse_update_colors) |
            0x12 => call!(Self::parse_particle) |
            0x13 => call!(Self::parse_damage) |
            0x14 => call!(Self::parse_spawn_static) |

            0x16 => call!(Self::parse_spawn_baseline) |
            0x17 => call!(Self::parse_temp_entity) |
            0x18 => call!(Self::parse_set_pause) |
            0x19 => call!(Self::parse_signon_num) |
            0x1A => call!(Self::parse_center_print) |
            0x1B => call!(Self::parse_killed_monster) |
            0x1C => call!(Self::parse_found_secret) |
            0x1D => call!(Self::parse_spawn_static_sound) |
            0x1E => call!(Self::parse_intermission) |
            0x1F => call!(Self::parse_finale) |
            0x20 => call!(Self::parse_cd_track) |
            0x21 => call!(Self::parse_sellscreen) |
            0x22 => call!(Self::parse_cutscreen) |
            0x23 => call!(Self::parse_show_lmp) |
            0x24 => call!(Self::parse_hide_lmp) |
            0x25 => call!(Self::parse_skybox) |

            0x32 => call!(Self::parse_download_data) |
            0x33 => call!(Self::parse_update_status_byte) |
            0x34 => call!(Self::parse_effect) |
            0x35 => call!(Self::parse_effect2) |
            0x36 => call!(Self::parse_precache) |
            0x37 => call!(Self::parse_spawn_baseline2) |
            0x38 => call!(Self::parse_spawn_static2) |
            0x39 => call!(Self::parse_entities) |
            0x3A => call!(Self::parse_csqc_entities) |
            0x3B => call!(Self::parse_spawn_static_sound2) |
            0x3C => call!(Self::parse_trail_particles) |
            0x3D => call!(Self::parse_point_particles) |
            0x3E => call!(Self::parse_point_particles1)
        )
    }
}

impl<'a> MessageSerilization<'a> for ServerMessage<'a> {
    fn serilize(&self, buffer: &'a mut [u8]) -> Option<usize> {
        let mut cursor = std::io::Cursor::new(buffer);
        match self {
            Self::Nop => {
                cursor.write_u8(0x01).ok()?;
            }
            Self::Disconnect => {
                cursor.write_u8(0x02).ok()?;
            }
            Self::UpdateStat { stat, value } => {
                cursor.write_u8(0x03).ok()?;
                cursor.write_u8(*stat).ok()?;
                cursor.write_u32::<BigEndian>(*value).ok()?;
            }
            Self::Version(version) => {
                cursor.write_u8(0x04).ok()?;
                cursor.write_u32::<BigEndian>(*version).ok()?;
            }
            Self::SetView(entity) => {
                cursor.write_u8(0x05).ok()?;
                cursor.write_u16::<BigEndian>(*entity).ok()?;
            }
            Self::Sound {
                sound,
                volume,
                attenuation,
                speed,
                entity,
                channel,
                looping,
            } => {
                cursor.write_u8(0x06).ok()?;
                // Dummy flag
                let flag_pos = cursor.position();
                cursor.write_u8(0x00).ok()?;
                let mut flags = 0x00;

                if let Some(volume) = volume {
                    flags |= SOUND_FIELD_FLAG_VOLUME;
                    cursor.write_u8(*volume).ok()?;
                }
                if let Some(attenuation) = attenuation {
                    flags |= SOUND_FIELD_FLAG_ATTENUATION;
                    cursor.write_u8((attenuation * 64.0) as u8).ok()?;
                }
                if let Some(speed) = speed {
                    flags |= SOUND_FIELD_FLAG_SPEEDUSHORT4000;
                    cursor
                        .write_u16::<BigEndian>((speed * 4000.0) as u16)
                        .ok()?;
                }
                if *entity > 0x1FFF || *channel > 0x07 {
                    flags |= SOUND_FIELD_FLAG_LARGE_ENTITY;
                    cursor.write_u16::<BigEndian>(*entity).ok()?;
                    cursor.write_u8(*channel).ok()?;
                } else {
                    let entchan = (entity << 3) | *channel as u16;
                    cursor.write_u16::<BigEndian>(entchan).ok()?;
                }
                if *sound > u8::MAX as u16 {
                    flags |= SOUND_FIELD_FLAG_LARGE_SOUND;
                    cursor.write_u16::<BigEndian>(*sound).ok()?;
                } else {
                    cursor.write_u8(*sound as u8).ok()?;
                }
                if *looping {
                    flags |= SOUND_FIELD_FLAG_LOOPING;
                }

                let end_pos = cursor.position();
                cursor.set_position(flag_pos);
                cursor.write_u8(flags).ok()?;
                cursor.set_position(end_pos);
            }
            Self::Time(time) => {
                cursor.write_u8(0x07).ok()?;
                cursor.write_f32::<LittleEndian>(*time).ok()?;
            }
            Self::Print(text) => {
                cursor.write_u8(0x08).ok()?;
                cursor.write(text.as_bytes()).ok()?;
                cursor.write_u8(0x00).ok()?;
            }
            Self::StuffText(text) => {
                cursor.write_u8(0x09).ok()?;
                cursor.write(text.as_bytes()).ok()?;
                cursor.write_u8(0x00).ok()?;
            }
            Self::SetAngle(a1, a2, a3) => {
                cursor.write_u8(0x0A).ok()?;
                write_angle16(&mut cursor, *a1).ok()?;
                write_angle16(&mut cursor, *a2).ok()?;
                write_angle16(&mut cursor, *a3).ok()?;
            }
            Self::ServerInfo {
                protocol,
                max_clients,
                game_type,
                world_message,
                models,
                sounds,
            } => {
                cursor.write_u8(0x0B).ok()?;
                cursor.write_u32::<BigEndian>(*protocol).ok()?;
                cursor.write_u8(*max_clients).ok()?;
                cursor.write_u8(*game_type).ok()?;
                cursor.write(world_message.as_bytes()).ok()?;
                cursor.write_u8(0x00).ok()?;
                for model in models {
                    cursor.write(model.as_bytes()).ok()?;
                    cursor.write_u8(0x00).ok()?;
                }
                cursor.write_u8(0x00).ok()?;
                for sound in sounds {
                    cursor.write(sound.as_bytes()).ok()?;
                    cursor.write_u8(0x00).ok()?;
                }
                cursor.write_u8(0x00).ok()?;
            }
            Self::LightStyle { index, style } => {
                cursor.write_u8(0x0C).ok()?;
                cursor.write_u8(*index).ok()?;
                cursor.write(style.as_bytes()).ok()?;
                cursor.write_u8(0x00).ok()?;
            }
            Self::UpdateName { client, name } => {
                cursor.write_u8(0x0D).ok()?;
                cursor.write_u8(*client).ok()?;
                cursor.write(name.as_bytes()).ok()?;
                cursor.write_u8(0x00).ok()?;
            }
            Self::UpdateFrags { client, frags } => {
                cursor.write_u8(0x0E).ok()?;
                cursor.write_u8(*client).ok()?;
                cursor.write_u16::<BigEndian>(*frags).ok()?;
            }
            // Self::ClientData => {

            // },
            Self::StopSound { entity, channel } => {
                if *entity > 0x1FFF || *channel > 0x07 {
                    // @todo Is there another type of packet that can handle bigger entity?
                    // like the flag SOUND_FIELD_FLAG_LARGE_ENTITY for the Sound packet?
                    eprintln!("To big entity or channel for StopSound packet!");
                    return None;
                }
                cursor.write_u8(0x0F).ok()?;

                let entchan = *entity << 3 | *channel as u16;
                cursor.write_u16::<BigEndian>(entchan).ok()?;
            }
            Self::SignonNum(stage) => {
                cursor.write_u8(0x19).ok()?;
                cursor.write_u8(*stage).ok()?;
            }

            Self::SpawnStaticSound {
                sound,
                origin,
                volume,
                attenuation,
            } => {
                let small = *sound <= std::u8::MAX as u16;
                if small {
                    cursor.write_u8(0x1D).ok()?;
                } else {
                    cursor.write_u8(0x3B).ok()?;
                }
                cursor.write_f32::<LittleEndian>(origin.0).ok()?;
                cursor.write_f32::<LittleEndian>(origin.1).ok()?;
                cursor.write_f32::<LittleEndian>(origin.2).ok()?;
                if small {
                    cursor.write_u8(*sound as u8).ok()?;
                } else {
                    cursor.write_u16::<BigEndian>(*sound).ok()?;
                }
                cursor.write_u8(*volume).ok()?;
                cursor.write_u8(*attenuation).ok()?;
            }

            Self::CDTrack { track, looptrack } => {
                cursor.write_u8(0x20).ok()?;
                cursor.write_u8(*track).ok()?;
                cursor.write_u8(*looptrack).ok()?;
            }

            _ => todo!(),
        }
        Some(cursor.position() as usize)
    }
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    use super::*;
    const EOF: &'static [u8] = b"";
    #[test]
    fn test_parse_string() {
        let input = b"hello\x00world\x00foo\x00";
        assert_eq!(
            tuple((parse_string, parse_string))(input),
            Ok((&b"foo\x00"[..], ("hello", "world")))
        );
    }

    #[test]
    fn test_angle16() {
        for (raw, res) in [
            (b"\x00\x00", 0.0),
            (b"\x7F\xFF", 179.9945),
            (b"\x80\x00", -180.0),
        ]
        .iter()
        {
            assert_eq!(parse_angle16(*raw), Ok((EOF, *res)));

            let mut buffer = [0u8; 2];
            write_angle16(&mut std::io::Cursor::new(&mut buffer[..]), *res).unwrap();
            assert_eq!(buffer, **raw);
        }
    }

    mod ClientMessage {
        use super::super::{ClientMessage, MessageParse, MessageSerilization};
        use super::EOF;
        fn test_packet(raw: &[u8], result: ClientMessage) {
            assert_eq!(
                ClientMessage::parse(raw),
                Ok((EOF, result.clone())),
                "Parsing failed"
            );

            let mut buffer = [0u8; 4096];
            let len = result.serilize(&mut buffer).unwrap();
            assert_eq!(&buffer[..len], raw, "Serilization failed");
        }

        fn test_packet_round_trip(message: ClientMessage) {
            let mut buffer = [0u8; 4096];
            let len = message.serilize(&mut buffer).unwrap();
            let buffer = &buffer[..len];

            let rt_message = ClientMessage::parse(buffer).unwrap().1;
            assert_eq!(message, rt_message);
        }

        #[test]
        fn Nop() {
            test_packet(b"\x01", ClientMessage::Nop);
        }

        #[test]
        fn Disconnect() {
            test_packet(b"\x02", ClientMessage::Disconnect);
        }

        #[test]
        fn Move() {
            test_packet(
                b"\x03\xAA\xAA\xAA\xAA\x42\xF6\xA4\x5A\x00\x00\x7F\xFF\x80\x00\x00\x6F\x00\xDE\x01\x4D\x12\x34\x56\x78\x77\x7F\xFF\x80\x01\x43\xDE\x38\xD5\x44\x0A\xE3\x85\x44\x26\xAA\xA0\x44\x42\x71\xBA\x44\x5E\x38\xD5\x44\x79\xFF\xF0\x43\x21",
                ClientMessage::Move {
                    sequence: 0xAAAAAAAA,
                    time: 123.321,
                    view_angle: (0.0, 179.9945, -180.0),
                    forward: 111.0,
                    sideways: 222.0,
                    vertical: 333.0,
                    buttons: 0x12345678,
                    impulse: 0x77,
                    cursor_screen: (1.0, -1.0),
                    cursor_start: (444.444, 555.555, 666.666),
                    cursor_impact: (777.777, 888.888, 999.999),
                    cursor_entity: 0x4321
                }
            )
        }

        #[test]
        fn StringCmd() {
            test_packet(
                b"\x04Hello world\x00",
                ClientMessage::StringCmd("Hello world"),
            );
        }
    }

    mod ServerMessage {
        use super::super::{MessageParse, MessageSerilization, ServerMessage};
        use super::EOF;

        fn test_packet(raw: &[u8], result: ServerMessage) {
            assert_eq!(
                ServerMessage::parse(raw),
                Ok((EOF, result.clone())),
                "Parsing failed"
            );

            let mut buffer = [0u8; 4096];
            let len = result.serilize(&mut buffer).unwrap();

            assert_eq!(&buffer[..len], raw, "Serilization failed");
        }

        fn test_packet_round_trip(message: ServerMessage) {
            let mut buffer = [0u8; 4096];
            let len = message.serilize(&mut buffer).unwrap();
            let buffer = &buffer[..len];

            let rt_message = ServerMessage::parse(buffer).unwrap().1;
            assert_eq!(message, rt_message);
        }

        #[test]
        fn Nop() {
            test_packet(b"\x01", ServerMessage::Nop);
        }

        #[test]
        fn Disconnect() {
            test_packet(b"\x02", ServerMessage::Disconnect);
        }

        #[test]
        fn UpdateStat() {
            test_packet(
                b"\x03\xFF\x12\x34\x56\x78",
                ServerMessage::UpdateStat {
                    stat: 0xFF,
                    value: 0x12345678,
                },
            );
        }

        #[test]
        fn Version() {
            test_packet(b"\x04\x12\x34\x56\x78", ServerMessage::Version(0x12345678));
        }

        #[test]
        fn SetView() {
            test_packet(b"\x05\x12\x34", ServerMessage::SetView(0x1234));
        }

        #[test]
        fn SetSound() {
            test_packet(
                // Test entity
                &[0x06, 0x00, 0b11111111u8, 0b11111_000, 0x77][..],
                ServerMessage::Sound {
                    sound: 0x77,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0b000,
                    entity: 0b11111111_11111,
                    looping: false,
                },
            );
            test_packet(
                // Test channel
                &[0x06, 0x00, 0x00, 0b00000_111, 0x77][..],
                ServerMessage::Sound {
                    sound: 0x77,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0b111,
                    entity: 0x00,
                    looping: false,
                },
            );
            test_packet(
                // Test volumne
                &[0x06, 0x01, 0x35, 0x00, 0b00000_111, 0x77][..],
                ServerMessage::Sound {
                    sound: 0x77,
                    volume: Some(0x35),
                    attenuation: None,
                    speed: None,
                    channel: 0b111,
                    entity: 0x00,
                    looping: false,
                },
            );
            test_packet(
                // Test attenuation
                &[0x06, 0x02, ((0.5) * 64.0) as u8, 0x00, 0b00000_111, 0x77][..],
                ServerMessage::Sound {
                    sound: 0x77,
                    volume: None,
                    attenuation: Some(0.5),
                    speed: None,
                    channel: 0b111,
                    entity: 0x00,
                    looping: false,
                },
            );
            test_packet(
                // Test looping
                &[0x06, 0x04, 0x00, 0b00000_111, 0x77][..],
                ServerMessage::Sound {
                    sound: 0x77,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0b111,
                    entity: 0x00,
                    looping: true,
                },
            );
            test_packet(
                // Test large entity
                &[0x06, 0x08, 0x12, 0x34, 0x56, 0x78][..],
                ServerMessage::Sound {
                    sound: 0x78,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0x56,
                    entity: 0x1234,
                    looping: false,
                },
            );
            test_packet(
                // Test large sound
                &[0x06, 0x10, 0x00, 0b00000_111, 0xAB, 0xCD][..],
                ServerMessage::Sound {
                    sound: 0xABCD,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0b111,
                    entity: 0x00,
                    looping: false,
                },
            );
            test_packet(
                // Test speed
                &[0x06, 0x20, 0xFF, 0xFF, 0x00, 0b00000_111, 0x77][..],
                ServerMessage::Sound {
                    sound: 0x77,
                    volume: None,
                    attenuation: None,
                    speed: Some((0xFFFFu16 as f32) / 4000.0),
                    channel: 0b111,
                    entity: 0x00,
                    looping: false,
                },
            );

            test_packet(
                // Test large sound edge cases (under)
                &[0x06, 0x00, 0x00, 0x00, 0xFF],
                ServerMessage::Sound {
                    sound: 0xFF,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0x00,
                    entity: 0x00,
                    looping: false,
                },
            );
            test_packet(
                // Test large sound edge cases (above)
                &[0x06, 0x10, 0x00, 0x00, 0x01, 0xFF],
                ServerMessage::Sound {
                    sound: 0x01FF,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0x00,
                    entity: 0x00,
                    looping: false,
                },
            );

            test_packet(
                // Test large entity (channel under)
                &[0x06, 0x00, 0x00, 0x07, 0x00],
                ServerMessage::Sound {
                    sound: 0x00,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0x07,
                    entity: 0x00,
                    looping: false,
                },
            );
            test_packet(
                // Test large entity (channel above)
                &[0x06, 0x08, 0x00, 0x00, 0x08, 0x00],
                ServerMessage::Sound {
                    sound: 0x00,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0x08,
                    entity: 0x0000,
                    looping: false,
                },
            );

            test_packet(
                // Test large entity (entity under)
                &[0x06, 0x00, 0xFF, 0xF8, 0x00],
                ServerMessage::Sound {
                    sound: 0x00,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0x00,
                    entity: 0x1FFF,
                    looping: false,
                },
            );

            test_packet(
                // Test large entity (entity over)
                &[0x06, 0x08, 0x20, 0x00, 0x00, 0x00],
                ServerMessage::Sound {
                    sound: 0x00,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0x00,
                    entity: 0x2000,
                    looping: false,
                },
            );

            // Test round trip for all entity values
            for entity in 0..0xFFFF {
                test_packet_round_trip(ServerMessage::Sound {
                    sound: 0x00,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0x00,
                    entity,
                    looping: false,
                });
            }
            // Test round trip for all channel values
            for channel in 0..0xFF {
                test_packet_round_trip(ServerMessage::Sound {
                    sound: 0x00,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel,
                    entity: 0x00,
                    looping: false,
                });
            }
            // Test round trip for all sound values
            for sound in 0..0xFFFF {
                test_packet_round_trip(ServerMessage::Sound {
                    sound,
                    volume: None,
                    attenuation: None,
                    speed: None,
                    channel: 0x00,
                    entity: 0x00,
                    looping: false,
                });
            }

            let all_flags = super::SOUND_FIELD_FLAG_VOLUME
                | super::SOUND_FIELD_FLAG_ATTENUATION
                | super::SOUND_FIELD_FLAG_LOOPING
                | super::SOUND_FIELD_FLAG_LARGE_ENTITY
                | super::SOUND_FIELD_FLAG_LARGE_SOUND
                | super::SOUND_FIELD_FLAG_SPEEDUSHORT4000;
            test_packet(
                // Check that all flags works together
                &[
                    0x06, all_flags, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0, 0x77,
                ][..],
                ServerMessage::Sound {
                    sound: 0xF077,
                    volume: Some(0x12),
                    attenuation: Some((0x34u8 as f32) / 64.0),
                    speed: Some((0x5678u16 as f32) / 4000.0),
                    channel: 0xDE,
                    entity: 0x9ABC,
                    looping: true,
                },
            );
            // Check that no unknown flags passes
            for flag in 0..std::u8::MAX {
                if (flag & !all_flags) != 0 {
                    let input = [
                        0x06, flag, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    ];
                    let result = ServerMessage::parse(&input[..]);
                    assert!(
                        result.is_err(),
                        "Expected parse error, got result = {:?}",
                        result
                    );
                }
            }
        }

        #[test]
        fn Time() {
            test_packet(b"\x07\x5a\xa4\xf6\x42", ServerMessage::Time(123.321));
        }

        #[test]
        fn Print() {
            test_packet(
                b"\x08hello world\n\x00",
                ServerMessage::Print("hello world\n"),
            );
        }

        #[test]
        fn StuffText() {
            test_packet(
                b"\x09hello world\n\x00",
                ServerMessage::StuffText("hello world\n"),
            );
        }

        #[test]
        fn SetAngle() {
            test_packet(
                b"\x0A\x00\x00\x7F\xFF\x80\x00",
                ServerMessage::SetAngle(0.0, 179.9945, -180.0),
            );
        }

        #[test]
        fn ServerInfo() {
            test_packet(
                b"\x0B\x12\x34\x56\x78\x55\x77Message\x00Model1\x00Model2\x00\x00Sound1\x00Sound2\x00\x00",
                ServerMessage::ServerInfo{
                    protocol: 0x12345678,
                    max_clients: 0x55,
                    game_type: 0x77,
                    world_message: "Message", 
                    models: vec!["Model1", "Model2"],
                    sounds: vec!["Sound1", "Sound2"]
                }
            )
        }

        #[test]
        fn LightStyle() {
            test_packet(
                b"\x0C\x55hello world\x00",
                ServerMessage::LightStyle {
                    index: 0x55,
                    style: "hello world",
                },
            )
        }

        #[test]
        fn UpdateName() {
            test_packet(
                b"\x0D\x55hello world\x00",
                ServerMessage::UpdateName {
                    client: 0x55,
                    name: "hello world",
                },
            );
        }

        #[test]
        fn UpdateFrags() {
            test_packet(
                b"\x0E\x55\x13\x37",
                ServerMessage::UpdateFrags {
                    client: 0x55,
                    frags: 0x1337,
                },
            );
        }

        #[test]
        fn SignonNum() {
            test_packet(b"\x19\x01", ServerMessage::SignonNum(0x01));
        }
        
        #[test]
        fn SpawnStaticSound() {
            // "small sound" packet
            test_packet(
                b"\x1D\xD5\x38\xDE\x42\xD5\x38\x5E\x43\xA0\xAA\xA6\x43\x12\x34\x56",
                ServerMessage::SpawnStaticSound{
                    origin: (111.111, 222.222, 333.333),
                    sound: 0x12,
                    volume: 0x34,
                    attenuation: 0x56
                }
            );
            // "large sound" packet
            test_packet(
                b"\x3B\xD5\x38\xDE\x42\xD5\x38\x5E\x43\xA0\xAA\xA6\x43\x12\x34\x56\x78",
                ServerMessage::SpawnStaticSound{
                    origin: (111.111, 222.222, 333.333),
                    sound: 0x1234,
                    volume: 0x56,
                    attenuation: 0x78
                }
            );

            // just under large sound
            test_packet(
                b"\x1D\xD5\x38\xDE\x42\xD5\x38\x5E\x43\xA0\xAA\xA6\x43\xFF\x34\x56",
                ServerMessage::SpawnStaticSound{
                    origin: (111.111, 222.222, 333.333),
                    sound: 0xFF,
                    volume: 0x34,
                    attenuation: 0x56
                }
            );

            // just over large sound
            test_packet(
                b"\x3B\xD5\x38\xDE\x42\xD5\x38\x5E\x43\xA0\xAA\xA6\x43\x01\x00\x34\x56",
                ServerMessage::SpawnStaticSound{
                    origin: (111.111, 222.222, 333.333),
                    sound: 0x100,
                    volume: 0x34,
                    attenuation: 0x56
                }
            );

            // Try all sound values
            for sound in 0..0xFFFF {
                test_packet_round_trip(
                    ServerMessage::SpawnStaticSound{
                        origin: (111.111, 222.222, 333.333),
                        sound,
                        volume: 0x34,
                        attenuation: 0x56

                    }
                );
            }
        }

        #[test]
        fn CDTrack() {
            test_packet(
                b"\x20\x12\x34",
                ServerMessage::CDTrack {
                    track: 0x12,
                    looptrack: 0x34,
                },
            );
        }
    }
}
